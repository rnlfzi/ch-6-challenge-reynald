const {User_History} = require('../models');
const {User_Game} = require('../models');

const getAllUserHistory= (req, res) => {
    User_History.findAll({
        attributes: ['id', 'game', 'result', 'user_id'],
        include: [{
            model: User_Game,
            as: 'user',
            attributes: ['id', 'username']
        }]
    })
    .then((user) => {
        res.status(200).json({
            msg: 'Yeayyy, Success Fetch All Data User History',
            data: user
        })
    })
    .catch((err) => {
        res.status(500).json({
            msg: "Sorry, Failed Fetch All Data User History",
            detail: err.message
        })
    })
}

const getUserHistoryById= (req, res) => {
    const {id} = req.params;
    User_History.findOne({
        where: {
            id: id
        },
        attributes: ['id', 'game', 'result', 'user_id'],
        include: [{
            model: User_Game,
            as: 'user',
            attributes: ['id', 'username']
        }]
    })
    .then((user) => {
        if(user === null) return res.status(404).json({
            msg: `Sorry, User History with Id : ${id} is not Found!!!`
        })

        res.status(200).json({
            msg: `Yeayyy, Success to Fetch data User History By Id: ${id}`,
            data: user
        })
    })
    .catch((err) => {
        res.status(500).json({
            msg: `Something is Wrong!!!`,
            detail: err.message
        })
    })
}

const getAllUserHistoryByUserId= (req, res) => {
    const {userId} = req.params;
    User_History.findAll({
        where: {
            user_id: userId
        },
        attributes: ['id', 'game', 'result', 'user_id'],
        include: [{
            model: User_Game,
            as: 'user',
            attributes: ['id', 'username']
        }]
    })
    .then((user) => {
        if(user.length === 0) return res.status(404).json({
            msg: `Sorry, User History with User Id: ${userId}, Is not Found!!!`
        })

        res.status(200).json({
            msg: `Yeayyy, Success to Fetch data User History By User Id: ${userId}`,
            data: user
        })
    })
    .catch((err) => {
        res.status(500).json({
            msg: `Something is Wrong!!!`,
            detail: err.message
        })
    })
}

const createUserHistory= (req, res) => {
    const {game, result, user_id} = req.body;

    User_Game.findOne({
        where: {
            id: user_id
        }
    })
    .then((user) => {
        if(user === null) return res.status(404).json({
            msg: 'Sorry, Id is doesnt exist'
        })

        User_History.create({
            game: game,
            result: result,
            user_id: user_id
        })
        .then((userHistory) => {
            res.status(201).json({
                msg: 'Yeayyy, Success to Create data User History',
                data: userHistory
            })
        })
    })
    .catch((err) => {
        res.status(500).json({
            msg: 'Sorry, Failed to Create data User History',
            detail: err.message
        })
    })
}

const updateUserHistory= (req, res) => {
    const {id} = req.params;
    const {game, result} = req.body;
    User_History.update({
        game: game,
        result: result
    }, {
        where: {
            id: id
        }
    })
    .then((user) => {
        if(user === null) return res.status(404).json({
            msg: `Sorry, User History with Id: ${id}, Is not Found!!!`
        })
        
        res.status(200).json({
            msg: `Yeayyy, Success to Update data User History with Id: ${id}`
        })
    })
    .catch((err) => {
        res.status(500).json({
            msg: `Something is Wrong!!!`,
            detail: err.message
        })
    })
}

const deleteUserHistory= (req, res) => {
    const {id} = req.params;
    User_History.destroy({
        where: {
            id: id
        }
    })
    .then((user) => {
        if(user === null) return res.status(404).json({
            msg: `Sorry, User History with Id: ${id}, Is not Found!!!`
        })

        res.status(200).json({
            msg: `Yeayyy, Success to Delete data User History with Id: ${id}`
        })
    })
    .catch((err) => {
        res.status(500).json({
            msg: `Something is Wrong!!!`,
            detail: err.message
        })
    })
}

const deleteAllUserHistoryByUserId = (req, res) => {
    const {userId} = req.params;
    User_History.destroy({
        where: {
            user_id: userId
        }
    })
    .then((user) => {
        if(user.length === 0) return res.status(404).json({
            msg: `Sorry, User History with Id: ${userId}, Is not Found!!!`
        })

        res.status(200).json({
            msg: `Yeayyy, Success to Delete All data User History with Id: ${userId}`
        })
    })
    .catch((err) => {
        res.status(500).json({
            msg: `Something is Wrong!!!`,
            detail: err.message
        })
    })
}

module.exports = {
    getAllUserHistory,
    getUserHistoryById,
    getAllUserHistoryByUserId,
    createUserHistory,
    updateUserHistory,
    deleteUserHistory,
    deleteAllUserHistoryByUserId
}