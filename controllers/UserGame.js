const {User_Game} = require('../models');
const {User_Bio} = require('../models');
const {User_History} = require('../models');

const getAllUser = (req, res) => {
    User_Game.findAll({
        attributes: ['id', 'username'],
        include: [{
            model: User_Bio,
            as: 'biodata',
            attributes: ['firstName', 'lastName', 'email']
        }, {
            model: User_History,
            as: 'history',
            attributes: ['game', 'result']
        }]
    })
    .then((user) => {
        res.status(200).json({
            msg: 'Yeayyy, Success to Fetch All Data User Game',
            data: user
        })
    })
    .catch((err) => {
        res.status(500).json({
            msg: 'Sorry, Failed to Fetch All Data User Game!!!',
            detail: err.message
        })
    })
}

const getUserById = (req, res) => {
    const {id} = req.params;
    User_Game.findOne({
        where: {
            id: id
        },
        attributes: ['id', 'username'],
        include: [{
            model: User_Bio,
            as: 'biodata',
            attributes: ['firstName', 'lastName', 'email']
        }, {
            model: User_History,
            as: 'history',
            attributes: ['game', 'result']
        }]
    })
    .then((user) => {
        if(user === null) return res.status(404).json({
            msg: `Sorry, User Game with Id : ${id} is not Found!!!`
        })

        res.status(200).json({
            msg: `Yeayyy, Success to Fetch User Game by Id: ${id}`,
            data: user
        })
    }) 
    .catch((err) => {
        res.status(500).json({
            msg: 'Something is Wrong!!!',
            detail: err.message
        })
    })
}

const createUser = (req, res) => {
    const {username, password, firstName, lastName, email} = req.body;
    User_Game.create({
        username: username,
        password: password
    })
    .then((user) => {
        User_Bio.create({
            firstName: firstName,
            lastName: lastName,
            email: email,
            user_id: user.id
        })
        res.status(201).json({
            msg: 'Yeayyy, Success to Create Data User',
            data: user
        })
    })
    .catch((err) => {
        res.status(500).json({
            msg: 'Sorry, Failed to Create Data User!!!',
            detail: err.message
        })
    })
}

const updateUser = (req, res) => {
    const {id} = req.params;
    const {username, password, firstName, lastName, email} = req.body;
    User_Game.update({
        username: username,
        password: password
    }, {
        where: {
            id: id
        }
    })
    User_Bio.update({
        firstName: firstName,
        lastName: lastName,
        email: email
    }, {
        where: {
            user_id: id
        }
    })
    .then((user) => {
        if(user === null) return res.status(404).json({
            msg: `Sorry, User Game with Id: ${id}, Is not Found!!!`
        })

        res.status(200).json({
            msg: `Yeayyy, Success to Update data User Game and Bio with Id: ${id}`
        })
    })
    .catch((err) => {
        res.status(500).json({
            msg: `Something is Wrong!!!`,
            detail: err.message
        })
    })
}

const deleteALlData = (req, res) => {
    const {id} = req.params;
    User_Game.destroy({
        where: {
            id: id
        }
    })
    User_Bio.destroy({
        where: {
            user_id: id
        }
    })
    User_History.destroy({
        where: {
            user_id: id
        }
    })
    .then((user) => {
        if(user === null) return res.status(404).json({
            msg: `Sorry, User Game with Id: ${id}, Is not Found!!!`
        });

        res.status(200).json({
            msg: `Yeayyy, Success to Delete data User Game with Id: ${id}, include Delete data User Bio and User History`
        });
    })
    .catch((err) => {
        res.status(500).json({
            msg: `Something is Wrong!!!`,
            detail: err.message
        })
    })
}

module.exports = {
    getAllUser,
    getUserById,
    createUser,
    updateUser,
    deleteALlData
}