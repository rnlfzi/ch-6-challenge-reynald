'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up (queryInterface, Sequelize) {
        return Promise.all([
            queryInterface.addColumn("User_Bios", "user_id", {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
            queryInterface.addColumn("User_Histories", "user_id", {
                type: Sequelize.INTEGER,
                allowNull: true,
            }),
        ]);
    },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
