'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class User_Game extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
          // define association here
          models.User_Game.hasOne(models.User_Bio, {
            foreignKey: 'user_id',
            as: 'biodata'
          });

          models.User_Bio.belongsTo(models.User_Game, {
              foreignKey: 'user_id',
              as: 'user'
          });

          models.User_Game.hasMany(models.User_History, {
              foreignKey: 'user_id',
              as: 'history'
          });

          models.User_History.belongsTo(models.User_Game, {
              foreignKey: 'user_id',
              as: 'user'
          })
        }
    }
    User_Game.init({
        username: DataTypes.STRING,
        password: DataTypes.STRING,
    }, {
        sequelize,
        modelName: 'User_Game',
    });
    return User_Game;
};