const express = require('express');
const { 
    getAllUser, 
    getUserById, 
    createUser, 
    updateUser, 
    deleteALlData
} = require('../controllers/UserGame');

const router = express.Router();

router.post('/users', createUser);
router.get('/users/all', getAllUser);
router.get('/user/:id', getUserById);
router.put('/user/:id', updateUser);
router.delete('/user/:id', deleteALlData);

module.exports = router;