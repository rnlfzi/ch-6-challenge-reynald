const express = require('express');
const {
    getAllUserHistory, 
    getUserHistoryById, 
    getAllUserHistoryByUserId, 
    createUserHistory, 
    updateUserHistory, 
    deleteUserHistory,
    deleteAllUserHistoryByUserId
} = require('../controllers/UserHistory');

const router = express.Router();

router.post('/users/histories', createUserHistory);
router.get('/users/histories/all', getAllUserHistory);
router.get('/users/history/:id', getUserHistoryById);
router.put('/users/history/:id', updateUserHistory);
router.delete('/users/history/:id', deleteUserHistory);
router.delete('/users/histories/all/:userId', deleteAllUserHistoryByUserId);
router.get('/users/histories/all/:userId', getAllUserHistoryByUserId);

module.exports = router;