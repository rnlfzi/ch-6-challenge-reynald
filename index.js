const express = require('express');
const UserRoute = require('./routes/UserRoute');
const UserHistoryRoute = require('./routes/UserHistoryRoute');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(UserRoute);
app.use(UserHistoryRoute);

app.use((req, res, next) => {
    res.status(404).json({
        msg: 'Sorry, Can not found this Page',
    })
    next()
})

app.listen(8000, () => console.log('server is up and runing in port 8000'));